//
//  Candidate.m
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import "Candidate.h"
#import "Experience.h"


@implementation Candidate

@dynamic middle;
@dynamic name;
@dynamic sex;
@dynamic surname;
@dynamic test_score;
@dynamic want_salary;
@dynamic section_key;
@dynamic experiences;

@end
