//
//  Experience.m
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import "Experience.h"
#import "Candidate.h"


@implementation Experience

@dynamic field;
@dynamic level;
@dynamic candidate;

@end
