//
//  Candidate.h
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 Очень интересно. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Experience;

@interface Candidate : NSManagedObject

@property (nonatomic, retain) NSString * middle;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSString * surname;
@property (nonatomic, retain) NSNumber * test_score;
@property (nonatomic, retain) NSNumber * want_salary;
@property (nonatomic, retain) NSString * section_key;
@property (nonatomic, retain) NSSet *experiences;
@end

@interface Candidate (CoreDataGeneratedAccessors)

- (void)addExperiencesObject:(Experience *)value;
- (void)removeExperiencesObject:(Experience *)value;
- (void)addExperiences:(NSSet *)values;
- (void)removeExperiences:(NSSet *)values;

@end
