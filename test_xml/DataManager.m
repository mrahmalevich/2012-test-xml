//
//  DataManager.m
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import "DataManager.h"

static DataManager *_sharedInstance = nil;

@interface DataManager (Private)
- (Candidate *)insertCandidateWithDict:(NSDictionary *)dict;
- (void)insertExperienceWithDict:(NSDictionary *)dict forCandidate:(Candidate *)candidate;
@end

@implementation DataManager
@synthesize managedObjectModel = __managedObjectModel;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

#pragma mark - memory managment
+ (DataManager*)sharedInstance
{
    if (_sharedInstance == nil) {
        _sharedInstance = [[super alloc] init];
    }
    return _sharedInstance;
}

- (void)dealloc
{
    [__managedObjectContext release];
    [__managedObjectModel release];
    [__persistentStoreCoordinator release];
    [super dealloc];
}

#pragma mark - core data stack
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
        [__managedObjectContext setUndoManager:nil];
    }
    return __managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"candidates" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];   
    return __managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{  
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
        
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - xml parser deleagate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"candidate"]) {
        _currentCandidate = [[self insertCandidateWithDict:attributeDict] retain];
    }
    if ([elementName isEqualToString:@"exp"]) {
        [self insertExperienceWithDict:attributeDict forCandidate:_currentCandidate];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{   
    if ([elementName isEqualToString:@"candidate"]) {
        [_currentCandidate release]; _currentCandidate = nil;
    }
}

#pragma mark - loading
- (void)loadCandidates
{
    NSURL *xmlURL = [[NSBundle mainBundle] URLForResource:@"candidates" withExtension:@"xml"];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:xmlURL];
    [parser setDelegate:self];
    [parser parse];
    [parser release];
}

- (Candidate *)insertCandidateWithDict:(NSDictionary *)dict
{
    Candidate *candidate = [NSEntityDescription insertNewObjectForEntityForName:@"Candidate" inManagedObjectContext:self.managedObjectContext];

    candidate.sex = [[dict valueForKey:@"sex"] isEqualToString:@"F"] ? @"Female" : @"Male";
    candidate.name = [dict valueForKey:@"name"];
    candidate.middle = [dict valueForKey:@"middle"];
    candidate.surname = [dict valueForKey:@"surname"];
    candidate.test_score = [NSNumber numberWithInteger:[[dict valueForKey:@"test_score"] integerValue]];
    candidate.want_salary = [NSNumber numberWithInteger:[[dict objectForKey:@"want_salary"] integerValue]];
    candidate.section_key = [[dict valueForKey:@"name"] substringToIndex:1];

    return candidate;
}

- (void)insertExperienceWithDict:(NSDictionary *)dict forCandidate:(Candidate *)candidate
{
    Experience *experience = [NSEntityDescription insertNewObjectForEntityForName:@"Experience" inManagedObjectContext:self.managedObjectContext];
    
    experience.field = [dict valueForKey:@"field"];
    experience.level = [NSNumber numberWithInteger:[[dict objectForKey:@"level"] integerValue]];
    experience.candidate = candidate;

    [candidate addExperiencesObject:experience];
}

#pragma mark - utils
- (NSFetchedResultsController *)candidatesController
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Candidate" inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSSortDescriptor *sortDescriptorName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSSortDescriptor *sortDescriptorSurname = [[NSSortDescriptor alloc] initWithKey:@"surname" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObjects:sortDescriptorName, sortDescriptorSurname, nil]];
    [sortDescriptorName release];
    [sortDescriptorSurname release];
    
    NSFetchedResultsController *controller = [[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"section_key" cacheName:nil] autorelease];
    [request release];
    
    NSError *error = nil;
    return [controller performFetch:&error] ? controller : nil;
}

@end
