//
//  DetailViewController.h
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Candidate *detailItem;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UILabel *name;
@property (nonatomic, retain) IBOutlet UILabel *sex;
@property (nonatomic, retain) IBOutlet UILabel *testScore;
@property (nonatomic, retain) IBOutlet UILabel *wantSalary;

@end
