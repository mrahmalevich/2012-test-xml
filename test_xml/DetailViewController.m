//
//  DetailViewController.m
//  test_xml
//
//  Created by Михаил Рахмалевич on 24.07.12.
//  Copyright (c) 2012 _My Company Name_. All rights reserved.
//

#import "DetailViewController.h"

@interface UITableViewCellFixedLayout : UITableViewCell
@end

@implementation UITableViewCellFixedLayout
- (void) layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x, self.textLabel.frame.origin.y, 260, 20);
    self.detailTextLabel.frame = CGRectMake(self.frame.size.width - 50, self.textLabel.frame.origin.y, 20, 20);
}
@end


@interface DetailViewController ()
@property (nonatomic, retain) NSArray *experiencesArray;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize detailItem = _detailItem, experiencesArray = _experiencesArray;
@synthesize name = _name, sex = _sex, wantSalary = _wantSalary, testScore = _testScore, tableView = _tableView, scrollView = _scrollView;

#pragma mark - memory managment
- (void)dealloc
{
    [_detailItem release];
    [_experiencesArray release];
    [_name release];
    [_sex release];
    [_wantSalary release];
    [_testScore release];
    [_tableView release];
    [_scrollView release];
    [super dealloc];
}

#pragma mark - Managing the detail item
- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        _name.text = [NSString stringWithFormat:@"%@ %@ %@", _detailItem.name, _detailItem.middle, _detailItem.surname];
        _sex.text = _detailItem.sex;
        _testScore.text = [_detailItem.test_score stringValue];
        _wantSalary.text = [_detailItem.want_salary stringValue];

        self.experiencesArray = [_detailItem.experiences allObjects];
        [_tableView reloadData];
        _tableView.frame = CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.contentSize.width, _tableView.contentSize.height);
        
        CGFloat padding = 5.0f;
        _scrollView.contentSize = CGSizeMake(_tableView.frame.size.width, _tableView.frame.origin.y + _tableView.contentSize.height + padding);
        
        [self.view setNeedsDisplay];
        [self.view setNeedsLayout];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_scrollView scrollRectToVisible:CGRectMake(0, 1, 1, 1) animated:NO];
    [self configureView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Candidate Details";
    }
    return self;
}

#pragma mark - table view
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_experiencesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseID = @"experience_table_cell";
    UITableViewCellFixedLayout *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[UITableViewCellFixedLayout alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    Experience *experience = [_experiencesArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = experience.field;
    cell.detailTextLabel.text = [experience.level stringValue];
    
    return cell;
}

							
@end
